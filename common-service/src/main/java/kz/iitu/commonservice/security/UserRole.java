package kz.iitu.commonservice.security;

public enum UserRole {
    CLIENT, ADMIN, CASHIER;
}