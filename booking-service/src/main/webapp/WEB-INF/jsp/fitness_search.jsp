<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Fitness</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/jumbotron/">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .btn-outline-success {
        margin-right: 10px !important;
      }

      .card {
        margin-right: 25px;
        margin-bottom: 20px;
      }

      .btn-secondary {
        border-color: #00b3b3;
        background-color: #00b3b3;
      }

    </style>
    <link href="jumbotron.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="/index">Ticketon</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/concert_page">Concert</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/fitness_page">Fitness</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/movie_page">Movie</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/museum_page">Museum</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/musical_page">Musical</a>
          </li>
        </ul>
         <form class="form-inline my-2 my-lg-0" method="get" action="fitness_search">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" name="keyword">
                <input type="submit" value="Search" class="btn btn-outline-success my-2 my-sm-0"/>
         </form>
         <a class="btn btn-outline-success my-2 my-sm-0" href="/signin" role="button">Sign In</a>
  </div>
</nav>

<main role="main">
  <div class="jumbotron">
    <div class="container">
      <h1 class="display-3">Fitness</h1>
      <p>website always presents a wide range of concerts, which are held in Kazakhstan, Russia and around the world. If you are interested in what concerts will be held in Almaty, Nur-Sultan (Astana), Karaganda or you are looking for a world star tour, you can find all the necessary information here. We will help you realize your dream to see the idol alive. And it doesn't matter who is the rock, pop singer or opera singer. Concerts in Prague, Barcelona, Madrid, Paris, Munich, Moscow, St. Petersburg are in great demand. Book your ticket in advance.</p>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="card" style="width: 18rem;">
        <img style="width: 100%; height: 50%" class="card-img-top" src="https://onlinehealth-tips.com/wp-content/uploads/2019/10/08-Fitness-Tips.jpg" alt="Card image cap">
        <c:forEach items="${results}" var ="result">
             <div class="card-body">
                  <h5 class="card-title">${result.name}</h5>
                  <p class="card-text">${result.type}</p>
                  <p class="card-text">${result.duration}</p>
                  <p><a class="btn btn-secondary" href="/fitness_details?id=${session.fitness.id}" role="button">View details</a></p>
             </div>
        </c:forEach>
      </div>
    </div>
    <hr>
  </div>
</main>

<footer class="container">
  <p>&copy; Ticketon 2020</p>
</footer>
</body>
</html>
