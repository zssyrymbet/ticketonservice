
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sessions</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/grid/">
	<link rel="stylesheet" type="text/css" href="${spring.mvc.view.prefix}/css/admin.css"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .themed-grid-col {
          padding-top: 8px;
          padding-bottom: 8px;
          background-color: rgba(86, 61, 124, .15);
          border: 1px solid rgba(86, 61, 124, .2);
      }
      .btn {
          margin-left: 10px;
      }
      .mb-sm-0, .my-sm-0 {
          margin-bottom: .5rem!important;
      }
      .start {
          display:flex
      }
      .bg-dark{
      	background: #00b3b3 !important;
      }
      .btn-outline-success {
      	color: #f8f9fa;
      	border-color: #f8f9fa;
      }
      .btn-outline-success:hover{
         color: #f8f9fa !important;
         border-color: #00b3b3 !important;
      }
      a {
        color:#00b3b3 !important;
      }
      .addition {
           padding: 10px;
           background-color: #fff;
      }
      .color{
        color: #000 !important;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand color" href="/index">Ticketon</a>
		<a class="btn addition my-2 my-sm-0" href="/index" role="button">Sign Out</a>
	</nav>

	<div class="container-fluid">
  		<div class="row">
    		<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      			<div class="sidebar-sticky pt-3">
        		<ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/users">
                            Users
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" href="/concerts">
                            Concerts
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/fitness">
                            Fitness
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/movies">
                            Movies
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/museums">
                            Museums
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/musicals">
                            Musicals
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/places">
                            Places
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/sessions">
                            Sessions
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/tickets">
                            Tickets
                        </a>
                    </li>
        		</ul>
      			</div>
    		</nav>

    		<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4 py-4">
        		 <div class="start">
        		     <h1 class="h2">Sessions</h1>
        		     <a class="btn btn-outline-success my-2 my-sm-0" href="/session_form" role="button">Add Session</a>
        		 </div>
        		 <div class="container">
        		     <div class="row">
                          <div class="col-1 col-sm-1 themed-grid-col">Time</div>
                          <div class="col-2 col-sm-2 themed-grid-col">Concert</div>
                          <div class="col-2 col-sm-2 themed-grid-col">Fitness</div>
                          <div class="col-2 col-sm-2 themed-grid-col">Movie</div>
                          <div class="col-2 col-sm-2 themed-grid-col">Museum</div>
                          <div class="col-2 col-sm-2 themed-grid-col">Musical</div>
                     </div>
                     <c:forEach items="${sessions}" var ="session">
                         <div class="row">
                             <div class="col-1 col-sm-1 themed-grid-col">${session.time}</div>
                             <div class="col-2 col-sm-2 themed-grid-col">${session.concert.name}</div>
                             <div class="col-2 col-sm-2 themed-grid-col">${session.fitness.name}</div>
                             <div class="col-2 col-sm-2 themed-grid-col">${session.movie.name}</div>
                             <div class="col-2 col-sm-2 themed-grid-col">${session.museum.name}</div>
                             <div class="col-2 col-sm-2 themed-grid-col">${session.musical.name}</div>
                             <a class="btn btn-outline-success my-2 my-sm-0" href="/delete_session?id=${session.id}" role="button">Delete</a>
                         </div>
                      </c:forEach>
    			</div>
    		</main>
  		</div>
	</div>
</body>
</html>