<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sign In</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/grid/">
	<link rel="stylesheet" type="text/css" href="${spring.mvc.view.prefix}/css/admin.css"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .themed-grid-col {
          padding-top: 8px;
          padding-bottom: 8px;
          background-color: rgba(86, 61, 124, .15);
          border: 1px solid rgba(86, 61, 124, .2);
      }
      .btn {
          margin-left: 10px;
          margin-bottom: 5px;
      }
      .mb-sm-0, .my-sm-0 {
          margin-bottom: .5rem!important;
      }
      	.form-control {
              min-height: 41px;
      		box-shadow: none;
      		border-color: #e6e6e6;
      	}
      	.form-control:focus {
      		border-color: #00c1c0;
      	}
          .form-control, .btn {
              border-radius: 3px;
          }
      	.signup-form {
      		width: 425px;
      		margin: 0 auto;
      		padding: 30px 0;
      	}
      	.signup-form h2 {
      		color: #333;
      		font-weight: bold;
              margin: 0 0 25px;
          }
          .signup-form form {
          	margin-top: 70px;
              background: #fff;
      		border: 1px solid #f4f4f4;
              box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
              padding: 40px 50px;
          }
      	.signup-form .form-group {
      		margin-bottom: 20px;
      	}
      	.signup-form label {
      		font-weight: normal;
      		font-size: 13px;
      	}
      	.signup-form input[type="checkbox"] {
      		margin-top: 2px;
      	}
          .signup-form .btn {
              font-size: 16px;
              font-weight: bold;
      		background: #00c1c0;
      		border: none;
      		min-width: 140px;
              outline: none !important;
          }
      	.signup-form .btn:hover, .signup-form .btn:focus {
      		background: #00b3b3;
      	}
      	.signup-form a {
      		color: #00c1c0;
      		text-decoration: none;
      	}
      	.signup-form a:hover {
      		text-decoration: underline;
      	}
      	.col-sm-2 {
      	    max-width: 68% !important;
      	}
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/index">Ticketon</a>
	</nav>
    <div align="center">
        <div class="signup-form">
            <form:form action="login" method="post" modelAttribute="client" class="signup-form">
                  <h2>Sign In</h2>
                  <div class="form-group">
                        <form:input path="username" class="col-2 col-sm-2 themed-grid-col" placeholder="Username"/>
                  </div>
                  <div class="form-group">
                        <form:input path="password" class="col-2 col-sm-2 themed-grid-col" placeholder="Password"/>
                  </div>
                  <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg">Sign In</button>
                  </div>
                  <div class="text-center">Don't have an account? <a href="/signup">SignUp here</a></div>
            </form:form>
        </div>
    </div>
</body>
</html>