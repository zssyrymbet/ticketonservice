package kz.iitu.ticketonbookingservice;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class TicketonBookingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketonBookingServiceApplication.class, args);
	}

}
