package kz.iitu.ticketonbookingservice.repository;
import kz.iitu.ticketonbookingservice.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> getAllBookingsByClientId(Long id);
    Booking getAllBookingsByTicketId(Long id);
    Booking getBookingById(Long id);
}
