package kz.iitu.ticketonbookingservice.repository;
import kz.iitu.ticketonbookingservice.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client getClientById(Long id);
    Client findByUsername(String name);
}
