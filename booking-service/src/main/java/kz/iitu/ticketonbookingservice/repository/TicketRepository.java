package kz.iitu.ticketonbookingservice.repository;
import kz.iitu.ticketonbookingservice.entity.Session;
import kz.iitu.ticketonbookingservice.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Session findTicketBySessionId(Long id);
    Ticket findTicketByPlace(String name);
    Ticket getTicketById(Long id);
}
