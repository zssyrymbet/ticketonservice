package kz.iitu.ticketonbookingservice.service.impl;
import kz.iitu.ticketonbookingservice.entity.Booking;
import kz.iitu.ticketonbookingservice.entity.Client;
import kz.iitu.ticketonbookingservice.entity.ClientBooking;
import kz.iitu.ticketonbookingservice.entity.Ticket;
import kz.iitu.ticketonbookingservice.repository.*;
import kz.iitu.ticketonbookingservice.service.BookingService;
import lombok.NonNull;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Override
    @Transactional
    public Booking createBooking(Booking booking) {
        double balance = booking.getClient().getBalance() - (booking.getQuantity() * booking.getTicket().getPrice());
        if(booking.getTicket().getQuantity() == 0){
            System.out.println("Quantity is 0");
        }else if(booking.getClient().getBalance() < (booking.getQuantity() * booking.getTicket().getPrice())){
            System.out.println("Balance not enough");
        }else{
            int quantity = booking.getTicket().getQuantity() - booking.getQuantity();
            Client client = clientRepository.getClientById(booking.getClient().getId());
            client.setBalance(balance);
            clientRepository.saveAndFlush(client);
            Ticket ticket = ticketRepository.getTicketById(booking.getTicket().getId());
            ticket.setQuantity(quantity);
            ticketRepository.saveAndFlush(ticket);
            bookingRepository.saveAndFlush(booking);
        }
        return booking;
    }

    @Override
    public ClientBooking getAllBookingsByClientId(Long id) {
        List<Booking> bookings = bookingRepository.getAllBookingsByClientId(id);
        ClientBooking clientBooking = new ClientBooking();
        clientBooking.setClientBookingList(bookings);
        return clientBooking;
    }

    @Override
    public Booking getAllBookingsByTicketId(Long id) {
        @NonNull Booking booking = bookingRepository.getAllBookingsByTicketId(id);
        return booking;
    }

    @Override
    public Booking getBookingById(Long id) {
        return bookingRepository.getBookingById(id);
    }

    @Override
    @Transactional
    public Map<String, Boolean> cancelBooking(Long id) {
        Booking booking = bookingRepository.getBookingById(id);
        double balance = booking.getClient().getBalance() + booking.getQuantity() * booking.getTicket().getPrice();
        int quantity = booking.getTicket().getQuantity() + booking.getQuantity();
        Client client = clientRepository.getClientById(booking.getClient().getId());
        client.setBalance(balance);
        clientRepository.saveAndFlush(client);
        Ticket ticket = ticketRepository.getTicketById(booking.getTicket().getId());
        ticket.setQuantity(quantity);
        ticketRepository.saveAndFlush(ticket);
        bookingRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("canceled", Boolean.TRUE);
        return response;
    }
}
