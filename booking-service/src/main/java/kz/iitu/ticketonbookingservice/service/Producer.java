package kz.iitu.ticketonbookingservice.service;

import kz.iitu.ticketonbookingservice.entity.BookingRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    private static final String TOPIC = "booking_requests";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public String bookingRequestNotify(BookingRequest bookingRequest) {
        System.out.println(String.format("----------- Producing booking request to notification service -> %s", bookingRequest));
        this.kafkaTemplate.send(TOPIC, "bookingRequest");
        return "Successfully";
    }
}
