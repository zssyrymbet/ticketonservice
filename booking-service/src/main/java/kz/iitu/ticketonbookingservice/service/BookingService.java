package kz.iitu.ticketonbookingservice.service;
import kz.iitu.ticketonbookingservice.entity.Booking;
import kz.iitu.ticketonbookingservice.entity.ClientBooking;
import java.util.Map;

public interface BookingService {
    Booking createBooking(Booking booking);
    ClientBooking getAllBookingsByClientId(Long id);
    Booking getAllBookingsByTicketId(Long id);
    Booking getBookingById(Long id);
    Map<String, Boolean> cancelBooking(Long id);
}
