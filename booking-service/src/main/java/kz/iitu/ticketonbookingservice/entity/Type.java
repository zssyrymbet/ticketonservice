package kz.iitu.ticketonbookingservice.entity;

public enum Type {
    MALE,
    FEMALE,
    STUDENT,
}
