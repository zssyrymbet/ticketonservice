package kz.iitu.ticketonbookingservice.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "clients")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private int card_number;

    private double balance;

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private Set<Booking> bookings;

    @JsonIgnore
    public Set<Booking> getBookings() {
        return bookings;
    }

    @JsonProperty
    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }
}
