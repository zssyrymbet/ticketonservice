package kz.iitu.ticketonbookingservice.entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingRequest implements Serializable {
    private Long clientId;
    private Booking booking;
}
