package kz.iitu.ticketonbookingservice.controller;
import kz.iitu.ticketonbookingservice.entity.Booking;
import kz.iitu.ticketonbookingservice.entity.ClientBooking;
import kz.iitu.ticketonbookingservice.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BookingsController {

    @Autowired
    private BookingService bookingService;

    @RequestMapping(value = "/save_booking", method = RequestMethod.POST)
    public String createBooking(@ModelAttribute("booking") Booking booking) {
        bookingService.createBooking(booking);
        return "redirect:/index";
    }

    @RequestMapping("/delete_booking")
    public String delBooking(@RequestParam Long id) {
        bookingService.cancelBooking(id);
        return "redirect:/index";
    }

    @RequestMapping(value = {"/client_page"}, method = RequestMethod.GET)
    public String clientBooking(Model model, Long id) {
        ClientBooking bookings = bookingService.getAllBookingsByClientId(id);
        model.addAttribute("bookings", bookings);
        return "client_page";
    }
}
