package kz.iitu.ticketonbookingservice.controller;
import kz.iitu.ticketonbookingservice.entity.Booking;
import kz.iitu.ticketonbookingservice.entity.ClientBooking;
import kz.iitu.ticketonbookingservice.service.BookingService;
import kz.iitu.ticketonbookingservice.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @PostMapping("/create")
    public void createBooking(@RequestBody Booking booking)  {
         bookingService.createBooking(booking);
    }

    @GetMapping("/client/{client_id}")
    public ClientBooking getAllBookingsByClientId(@PathVariable(value = "client_id") Long client_id) {
        return bookingService.getAllBookingsByClientId(client_id);
    }

    @GetMapping("/ticket/{ticket_id}")
    public Booking getAllBookingsByTicketId( @PathVariable(value = "ticket_id") Long ticket_id) {
        return bookingService.getAllBookingsByTicketId(ticket_id);
    }

    @GetMapping("/id/{id}")
    public Booking getBookingById(@PathVariable(value = "id") Long id) {
        return bookingService.getBookingById(id);
    }

    @DeleteMapping("/cancel/{id}")
    public Map<String, Boolean> cancelBooking(@PathVariable(value = "id") Long id) {
        return bookingService.cancelBooking(id);
    }
}
