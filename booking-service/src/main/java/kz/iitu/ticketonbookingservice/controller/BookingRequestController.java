package kz.iitu.ticketonbookingservice.controller;
import kz.iitu.ticketonbookingservice.entity.Booking;
import kz.iitu.ticketonbookingservice.entity.BookingRequest;
import kz.iitu.ticketonbookingservice.service.BookingService;
import kz.iitu.ticketonbookingservice.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/booking/request")
public class BookingRequestController {

    private Producer producer;
    private BookingService bookingService;

    @Autowired
    public BookingRequestController(Producer producer, BookingService bookingService) {
        this.producer = producer;
        this.bookingService = bookingService;
    }

    @GetMapping
    public String sendMessageToKafkaTopic2(@RequestParam("clientId") Long clientId,
                                           @RequestParam("bookingId") Long bookingId) {

        Booking booking = bookingService.getBookingById(bookingId);
        BookingRequest bookingRequest = new BookingRequest(clientId, booking);
        this.producer.bookingRequestNotify(bookingRequest);
        return "Your request sent successful!";
    }
}
