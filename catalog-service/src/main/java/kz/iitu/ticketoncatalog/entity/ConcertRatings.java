package kz.iitu.ticketoncatalog.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ConcertRatings {

    private List<Rating> concertRatingList;

    public ConcertRatings() {}

    public ConcertRatings(List<Rating> concertSessionList) {
        this.concertRatingList = concertRatingList;
    }
}
