package kz.iitu.ticketoncatalog.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MusicalSessions {

    private List<Session> musicalSessionList;

    public MusicalSessions() {}

    public MusicalSessions(List<Session> musicalSessionList) {
        this.musicalSessionList = musicalSessionList;
    }
}
