package kz.iitu.ticketoncatalog.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MovieSessions {

    private List<Session> movieSessionList;

    public MovieSessions() {}

    public MovieSessions(List<Session> movieSessionList) {
        this.movieSessionList = movieSessionList;
    }
}
