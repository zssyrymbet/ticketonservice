package kz.iitu.ticketoncatalog.entity;

public enum Type {
    MALE,
    FEMALE,
    STUDENT,
}
