package kz.iitu.ticketoncatalog.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FitnessSessions {
    private List<Session> fitnessSessionList;

    public FitnessSessions() {

    }

    public FitnessSessions(List<Session> fitnessSessionList) {
        this.fitnessSessionList = fitnessSessionList;
    }
}
