package kz.iitu.ticketoncatalog.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "concerts")
public class Concert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private String location;

    @OneToMany(mappedBy = "concert", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Session> sessions;

    @OneToMany(mappedBy = "concert", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Rating> ratings;

    @JsonIgnore
    public Set<Session> getSessions() {
        return sessions;
    }

    @JsonProperty
    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }

    @JsonIgnore
    public Set<Rating> getRatings() {
        return ratings;
    }

    @JsonProperty
    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }
}

