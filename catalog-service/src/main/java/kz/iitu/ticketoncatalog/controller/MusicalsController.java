package kz.iitu.ticketoncatalog.controller;
import kz.iitu.ticketoncatalog.entity.Musical;
import kz.iitu.ticketoncatalog.service.MusicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class MusicalsController {

    @Autowired
    private MusicalService musicalService;

    @RequestMapping(value = "/save_musical", method = RequestMethod.POST)
    public String createMusical(@ModelAttribute("musical") Musical musical) {
        musicalService.createMusical(musical);
        return "redirect:/musicals";
    }

    @RequestMapping("/musical_form")
    public String showMusicalForm(Map<String, Object> model) {
        Musical musical = new Musical();
        model.put("musical", musical);
        return "new_musical";
    }

    @RequestMapping("/delete_musical")
    public String delMusical(@RequestParam Long id) {
        musicalService.deleteMusical(id);
        return "redirect:/musicals";
    }

    @RequestMapping(value = { "/musicals" }, method = RequestMethod.GET)
    public String getAllMusicals(Model model) {
        List<Musical> musicals = musicalService.getAllMusicals();
        model.addAttribute("musicals", musicals);
        return "musicals";
    }

//    @RequestMapping("/musical_search")
//    public String searchMusical(@RequestParam String keyword, Model model) {
//        List<Musical> results = musicalService.searchMusical(keyword);
//        model.addAttribute("results", results);
//        return "musical_search";
//    }
}
