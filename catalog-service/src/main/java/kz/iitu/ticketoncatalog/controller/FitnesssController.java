package kz.iitu.ticketoncatalog.controller;
import kz.iitu.ticketoncatalog.entity.Fitness;
import kz.iitu.ticketoncatalog.service.FitnessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class FitnesssController {

    @Autowired
    private FitnessService fitnessService;

    @RequestMapping(value = "/save_fitness", method = RequestMethod.POST)
    public String createFitness(@ModelAttribute("fitness") Fitness fitness) {
        fitnessService.createFitness(fitness);
        return "redirect:/fitness";
    }

    @RequestMapping("/fitness_form")
    public String showFitnessForm(Map<String, Object> model) {
        Fitness fitness = new Fitness();
        model.put("fitness", fitness);
        return "new_fitness";
    }

    @RequestMapping("/delete_fitness")
    public String delFitness(@RequestParam Long id) {
        fitnessService.deleteFitness(id);
        return "redirect:/fitness";
    }


    @RequestMapping(value = { "/fitness" }, method = RequestMethod.GET)
    public String getAllFitness(Model model) {
        List<Fitness> fitness = fitnessService.getAllFitness();
        model.addAttribute("fitness", fitness);
        return "fitness";
    }

//    @RequestMapping("/fitness_search")
//    public String searchFitness(@RequestParam String keyword, Model model) {
//        List<Fitness> result = fitnessService.searchFitness(keyword);
//        model.addAttribute("results", result);
//        return "fitness_search";
//    }
}

