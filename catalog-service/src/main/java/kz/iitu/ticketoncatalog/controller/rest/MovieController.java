package kz.iitu.ticketoncatalog.controller.rest;
import kz.iitu.ticketoncatalog.entity.Movie;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @PostMapping("/admin/create")
    public Movie createMovie(@RequestBody Movie movie)  {
        return movieService.createMovie(movie);
    }

    @GetMapping("/id/{id}")
    public Movie getMovieById(@PathVariable(value = "id") Long id) {
        return movieService.getMovieById(id);
    }

    @GetMapping("/all")
    public List<Movie> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping("/description/{description}")
    public Movie getMovieByDescription(@PathVariable(value = "description") String description) {
        return movieService.getMovieByDescription(description);
    }

    @GetMapping("/name/{name}")
    public Movie getMovieByName(@PathVariable(value = "name") String name) {
        return movieService.getMovieByName(name);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteMovie(@PathVariable(value = "id") Long id) {
        return movieService.deleteMovie(id);
    }

    @GetMapping("/session")
    public List<Session> findBySessionsIsNotNull(){
        return movieService.findBySessionsIsNotNull();
    }
}
