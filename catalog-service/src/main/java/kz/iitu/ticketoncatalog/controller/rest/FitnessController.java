package kz.iitu.ticketoncatalog.controller.rest;
import kz.iitu.ticketoncatalog.entity.Duration;
import kz.iitu.ticketoncatalog.entity.Fitness;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.entity.Type;
import kz.iitu.ticketoncatalog.service.FitnessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/fitness")
public class FitnessController {

    @Autowired
    private FitnessService fitnessService;

    @PostMapping("/admin/create")
    public void createFitness(@RequestBody Fitness fitness)  {
        fitnessService.createFitness(fitness);
    }

    @GetMapping("/id/{id}")
    public Fitness getFitnessById(@PathVariable(value = "id") Long id) {
        return fitnessService.getFitnessById(id);
    }

    @GetMapping("/all")
    public List<Fitness> getAllFitness() {
        return fitnessService.getAllFitness();
    }

    @GetMapping("/description/{description}")
    public Fitness getFitnessByDescription(@PathVariable(value = "description") String description) {
        return fitnessService.getFitnessByDescription(description);
    }

    @GetMapping("/name/{name}")
    public Fitness getFitnessByName(@PathVariable(value = "name") String name) {
        return fitnessService.getFitnessByName(name);
    }

    @GetMapping("/duration")
    public List<Fitness> getAllFitnessByDuration(@RequestBody Duration duration) {
        return fitnessService.getAllFitnessByDuration(duration);
    }

    @GetMapping("/museum")
    public List<Fitness> getAllFitnessByType(@RequestBody Type type) {
        return fitnessService.getAllFitnessByType(type);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteFitness(@PathVariable(value = "id") Long id) {
        return fitnessService.deleteFitness(id);
    }

    @GetMapping("/session")
    public List<Session> findBySessionsIsNotNull(){
        return fitnessService.findBySessionsIsNotNull();
    }
}

