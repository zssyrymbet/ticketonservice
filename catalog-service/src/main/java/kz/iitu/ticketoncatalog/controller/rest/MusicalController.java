package kz.iitu.ticketoncatalog.controller.rest;
import kz.iitu.ticketoncatalog.entity.Musical;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.service.MusicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/musical")
public class MusicalController {

    @Autowired
    private MusicalService musicalService;

    @PostMapping("/admin/create")
    public void createMusical(@RequestBody Musical musical)  {
        musicalService.createMusical(musical);
    }

    @GetMapping("/id/{id}")
    public Musical getMusicalById(@PathVariable(value = "id") Long id) {
        return musicalService.getMusicalById(id);
    }

    @GetMapping("/all")
    public List<Musical> getAllMusicals() {
        return musicalService.getAllMusicals();
    }

    @GetMapping("/description/{description}")
    public Musical getMusicalByDescription(@PathVariable(value = "description") String description) {
        return musicalService.getMusicalByDescription(description);
    }

    @GetMapping("/name/{name}")
    public Musical getMusicalByName(@PathVariable(value = "name") String name) {
        return musicalService.getMusicalName(name);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteMusical( @PathVariable(value = "id") Long id) {
        return musicalService.deleteMusical(id);
    }

    @GetMapping("/session")
    public List<Session> findBySessionsIsNotNull(){
        return musicalService.findBySessionsIsNotNull();
    }
}
