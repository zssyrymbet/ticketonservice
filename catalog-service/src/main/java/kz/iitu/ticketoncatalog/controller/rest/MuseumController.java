package kz.iitu.ticketoncatalog.controller.rest;
import kz.iitu.ticketoncatalog.entity.Museum;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.service.MuseumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/museum")
public class MuseumController {

    @Autowired
    private MuseumService museumService;

    @PostMapping("/admin/create")
    public void createMuseum(@RequestBody Museum museum)  {
        museumService.createMuseum(museum);
    }

    @GetMapping("/id/{id}")
    public Museum getMuseumById(@PathVariable(value = "id") Long id) {
        return museumService.getMuseumById(id);
    }

    @GetMapping("/all")
    public List<Museum> getAllMuseums() {
        return museumService.getAllMuseums();
    }

    @GetMapping("/description/{description}")
    public Museum getMuseumByDescription(@PathVariable(value = "description") String description) {
        return museumService.getMuseumByDescription(description);
    }

    @GetMapping("/name/{name}")
    public Museum getMuseumByName(@PathVariable(value = "name") String name) {
        return museumService.getMuseumByName(name);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteMuseum(@PathVariable(value = "id") Long id) {
        return museumService.deleteMuseum(id);
    }

    @GetMapping("/session")
    public List<Session> findBySessionsIsNotNull(){
        return museumService.findBySessionsIsNotNull();
    }
}

