package kz.iitu.ticketoncatalog.controller;
import kz.iitu.ticketoncatalog.entity.Concert;
import kz.iitu.ticketoncatalog.service.ConcertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class ConcertsController {

    @Autowired
    private ConcertService concertService;

    @RequestMapping(value = "/save_concert", method = RequestMethod.POST)
    public String createConcert(@ModelAttribute("concert") Concert concert) {
        concertService.createConcert(concert);
        return "redirect:/concerts";
    }

    @RequestMapping("/concert_form")
    public String showConcertForm(Map<String, Object> model) {
        Concert concert = new Concert();
        model.put("concert", concert);
        return "new_concert";
    }

    @RequestMapping(value = { "/concerts" }, method = RequestMethod.GET)
    public String getAllConcerts(Model model) {
        List<Concert> concerts = concertService.getAllConcerts();
        model.addAttribute("concerts", concerts);
        return "concerts";
    }

    @RequestMapping("/delete_concert")
    public String delConcert(@RequestParam Long id) {
        concertService.deleteConcert(id);
        return "redirect:/concerts";
    }

//    @RequestMapping("/concert_search")
//    public String searchConcert(@RequestParam String keyword, Model model) {
//        List<Concert> results = concertService.searchConcert(keyword);
//        model.addAttribute("results", results);
//        return "concert_search";
//    }

}

