package kz.iitu.ticketoncatalog.controller;

import kz.iitu.ticketoncatalog.entity.Movie;
import kz.iitu.ticketoncatalog.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class MoviesController {

    @Autowired
    private MovieService movieService;

    @RequestMapping(value = "/save_movie", method = RequestMethod.POST)
    public String createMovie(@ModelAttribute("movie") Movie movie) {
        movieService.createMovie(movie);
        return "redirect:/movies";
    }

    @RequestMapping("/movie_form")
    public String showMovieForm(Map<String, Object> model) {
        Movie movie = new Movie();
        model.put("movie", movie);
        return "new_movie";
    }

    @RequestMapping("/delete_movie")
    public String delMovie(@RequestParam Long id) {
        movieService.deleteMovie(id);
        return "redirect:/movies";
    }

    @RequestMapping(value = { "/movies" }, method = RequestMethod.GET)
    public String getAllMovies(Model model) {
        List<Movie> movies = movieService.getAllMovies();
        model.addAttribute("movies", movies);
        return "movies";
    }

//    @RequestMapping("/movie_search")
//    public String searchMovie(@RequestParam String keyword, Model model) {
//        List<Movie> results = movieService.searchMovie(keyword);
//        model.addAttribute("results", results);
//        return "movie_search";
//    }
}

