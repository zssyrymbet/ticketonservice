package kz.iitu.ticketoncatalog.controller.rest;
import kz.iitu.ticketoncatalog.entity.Concert;
import kz.iitu.ticketoncatalog.entity.Rating;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.service.ConcertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/concert")
public class ConcertController {

    @Autowired
    private ConcertService concertService;

    @PostMapping("/admin/create")
    public void createConcert(@RequestBody Concert concert) {
        concertService.createConcert(concert);
    }

    @GetMapping("/id/{id}")
    public Concert getConcertById(@PathVariable(value = "id") Long id) {
        return concertService.getConcertById(id);
    }

    @GetMapping("/all")
    public List<Concert> getAllConcerts() {
        return concertService.getAllConcerts();
    }

    @GetMapping("/location/{location}")
    public Concert getConcertByLocation(@PathVariable(value = "location") String location) {
        return concertService.getConcertByLocation(location);
    }

    @GetMapping("/name/{name}")
    public Concert getConcertByName(@PathVariable(value = "name") String name) {
        return concertService.getConcertByName(name);
    }

    @GetMapping("/description/{description}")
    public Concert getConcertByDescription(@PathVariable(value = "description") String description) {
        return concertService.getConcertByDescription(description);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteConcert(@PathVariable(value = "id") Long id) {
        return concertService.deleteConcert(id);
    }

    @GetMapping("/session")
    public List<Session> findBySessionsIsNotNull(){
        return concertService.findBySessionsIsNotNull();
    }

    @GetMapping("/rating")
    public List<Rating> findByRatingsIsNotNull(){
        return concertService.findByRatingsIsNotNull();
    }
}