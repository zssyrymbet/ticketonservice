package kz.iitu.ticketoncatalog.controller;

import kz.iitu.ticketoncatalog.entity.Museum;
import kz.iitu.ticketoncatalog.service.MuseumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class MuseumsController {

    @Autowired
    private MuseumService museumService;

    @RequestMapping(value = "/save_museum", method = RequestMethod.POST)
    public String createMuseum(@ModelAttribute("museum") Museum museum) {
        museumService.createMuseum(museum);
        return "redirect:/museums";
    }

    @RequestMapping("/museum_form")
    public String showMuseumForm(Map<String, Object> model) {
        Museum museum = new Museum();
        model.put("museum", museum);
        return "new_museum";
    }

    @RequestMapping("/delete_museum")
    public String delMuseum(@RequestParam Long id) {
        museumService.deleteMuseum(id);
        return "redirect:/museums";
    }

    @RequestMapping(value = { "/museums" }, method = RequestMethod.GET)
    public String getAllMuseums(Model model) {
        List<Museum> museums = museumService.getAllMuseums();
        model.addAttribute("museums", museums);
        return "museums";
    }

//    @RequestMapping("/museum_search")
//    public String searchMuseum(@RequestParam String keyword, Model model) {
//        List<Museum> results= museumService.searchMuseum(keyword);
//        model.addAttribute("results", results);
//        return "museum_search";
//    }
}

