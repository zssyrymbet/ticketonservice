package kz.iitu.ticketoncatalog.service;
import kz.iitu.ticketoncatalog.entity.Concert;
import kz.iitu.ticketoncatalog.entity.Rating;
import kz.iitu.ticketoncatalog.entity.Session;
import java.util.List;
import java.util.Map;

public interface ConcertService {
    Concert createConcert(Concert concert);
    Concert getConcertById(Long id);
    List<Concert> getAllConcerts();
    Concert getConcertByLocation(String location);
    Concert getConcertByName(String name);
    Concert getConcertByDescription(String description);
    Map<String, Boolean> deleteConcert(Long id);
    List<Session> findBySessionsIsNotNull();
    List<Rating> findByRatingsIsNotNull();
}
