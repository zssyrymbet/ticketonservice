package kz.iitu.ticketoncatalog.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketoncatalog.entity.*;
import kz.iitu.ticketoncatalog.repository.MusicalRepository;
import kz.iitu.ticketoncatalog.service.MusicalService;
import lombok.NonNull;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MusicalServiceImpl implements MusicalService {

    @Autowired
    private MusicalRepository musicalRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Musical createMusical(Musical musical) {
        musicalRepository.saveAndFlush(musical);
        return musical;
    }

    @Override
    public List<Musical> getAllMusicals() {
        return musicalRepository.findAll();
    }

    @Override
    public Musical getMusicalById(Long id) {
        return musicalRepository.getMusicalById(id);
    }

    @Override
    public Musical getMusicalByDescription(String description) {
        @NonNull Musical musical = musicalRepository.getMusicalByDescription(description);
        return musical;
    }

    @Override
    public Musical getMusicalName(String name) {
        @NonNull Musical musical = musicalRepository.getMusicalByName(name);
        return musical;
    }

    @Override
    public Map<String, Boolean> deleteMusical(Long id) {
        musicalRepository.findById(id);
        musicalRepository.deleteById(id);
        var response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackSession",
            threadPoolKey = "sessionMusicalInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public List<Session> findBySessionsIsNotNull() {
        String apiCredentials = "rest-session:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        MusicalSessions musicalSessions = restTemplate.exchange("http://session-service/session/musical", HttpMethod.GET, entity, MusicalSessions.class).getBody();
        return musicalSessions != null ? musicalSessions.getMusicalSessionList() : null;
    }

    public List<Session> getFallbackSession() {
        Session session = new Session();
        session.setId(0l);
        session.setTime("00:00:00");
        Musical musical = new Musical();
        musical.setId(0l);
        musical.setName("unknown");
        musical.setDescription("unknown");
        session.setMuseum(null);
        session.setMovie(null);
        session.setFitness(null);
        session.setConcert(null);
        return Arrays.asList(session);
    }
}
