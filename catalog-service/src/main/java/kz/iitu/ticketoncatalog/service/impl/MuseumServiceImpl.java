package kz.iitu.ticketoncatalog.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketoncatalog.entity.*;
import kz.iitu.ticketoncatalog.repository.MuseumRepository;
import kz.iitu.ticketoncatalog.service.MuseumService;
import lombok.NonNull;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MuseumServiceImpl implements MuseumService {

    @Autowired
    private MuseumRepository museumRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Museum createMuseum(Museum museum) {
        museumRepository.saveAndFlush(museum);
        return museum;
    }

    @Override
    public List<Museum> getAllMuseums() {
        return museumRepository.findAll();
    }

    @Override
    public Museum getMuseumById(Long id) {
        return museumRepository.getMuseumById(id);
    }

    @Override
    public Museum getMuseumByDescription(String description) {
        @NonNull Museum museum = museumRepository.getMuseumByDescription(description);
        return museum;
    }

    @Override
    public Museum getMuseumByName(String name) {
        @NonNull Museum museum = museumRepository.getMuseumByName(name);
        return museum;
    }

    @Override
    public Map<String, Boolean> deleteMuseum(Long id) {
        museumRepository.findById(id);
        museumRepository.deleteById(id);
        var response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackSession",
            threadPoolKey = "sessionMuseumInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public List<Session> findBySessionsIsNotNull() {
        String apiCredentials = "rest-session:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        MuseumSessions museumSessions = restTemplate.exchange("http://session-service/session/museum", HttpMethod.GET, entity, MuseumSessions.class).getBody();
        return museumSessions != null ? museumSessions.getMuseumSessionList() : null;
    }

    public List<Session> getFallbackSession() {
        Session session = new Session();
        session.setId(0l);
        session.setTime("00:00:00");
        Museum museum = new Museum();
        museum.setId(0l);
        museum.setName("unknown");
        museum.setDescription("unknown");
        session.setMusical(null);
        session.setMovie(null);
        session.setFitness(null);
        session.setConcert(null);
        return Arrays.asList(session);
    }
}

