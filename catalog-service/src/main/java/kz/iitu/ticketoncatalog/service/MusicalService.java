package kz.iitu.ticketoncatalog.service;
import kz.iitu.ticketoncatalog.entity.Musical;
import kz.iitu.ticketoncatalog.entity.Session;
import org.springframework.http.ResponseEntity;
import java.util.List;
import java.util.Map;

public interface MusicalService {
    Musical createMusical(Musical musical);
    List<Musical> getAllMusicals();
    Musical getMusicalById(Long id);
    Musical getMusicalByDescription(String description);
    Musical getMusicalName(String name);
    Map<String, Boolean> deleteMusical(Long id);
    List<Session> findBySessionsIsNotNull();
}
