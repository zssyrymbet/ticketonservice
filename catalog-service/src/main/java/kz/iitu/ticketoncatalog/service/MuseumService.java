package kz.iitu.ticketoncatalog.service;
import kz.iitu.ticketoncatalog.entity.Museum;
import kz.iitu.ticketoncatalog.entity.Session;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface MuseumService {
    Museum createMuseum(Museum museum);
    List<Museum> getAllMuseums();
    Museum getMuseumById(Long id);
    Museum getMuseumByDescription(String description);
    Museum getMuseumByName(String name);
    Map<String, Boolean> deleteMuseum(Long id);
    List<Session> findBySessionsIsNotNull();
}
