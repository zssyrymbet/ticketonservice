package kz.iitu.ticketoncatalog.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketoncatalog.entity.*;
import kz.iitu.ticketoncatalog.repository.MovieRepository;
import kz.iitu.ticketoncatalog.service.MovieService;
import lombok.NonNull;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.ParameterizedTypeReference;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Movie createMovie(Movie movie) {
        movieRepository.saveAndFlush(movie);
        return movie;
    }

    @Override
    public Movie getMovieById(Long id) {
        return movieRepository.getMovieById(id);
    }

    @Override
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    @Override
    public Movie getMovieByDescription(String description) {
        @NonNull Movie movie = movieRepository.getMovieByDescription(description);
        return movie;
    }

    @Override
    public Movie getMovieByName(String name) {
        @NonNull Movie movie = movieRepository.getMovieByName(name);
        return movie;
    }

    @Override
    public Map<String, Boolean> deleteMovie(Long id) {
        movieRepository.findById(id);
        movieRepository.deleteById(id);
        var response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackSession",
            threadPoolKey = "sessionMovieInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public List<Session> findBySessionsIsNotNull() {
        String apiCredentials = "rest-session:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        MovieSessions movieSessions = restTemplate.exchange("http://session-service/session/movie", HttpMethod.GET, entity, MovieSessions.class).getBody();
        return movieSessions != null ? movieSessions.getMovieSessionList() : null;
    }

    public List<Session> getFallbackSession() {
        Session session = new Session();
        session.setId(0l);
        session.setTime("00:00:00");
        Movie movie = new Movie();
        movie.setId(0l);
        movie.setName("unknown");
        movie.setDescription("unknown");
        movie.setCounty("unknown");
        movie.setYear(0);
        session.setMusical(null);
        session.setMuseum(null);
        session.setFitness(null);
        session.setConcert(null);
        return Arrays.asList(session);
    }
}
