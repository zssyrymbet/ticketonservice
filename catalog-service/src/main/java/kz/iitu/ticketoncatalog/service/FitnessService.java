package kz.iitu.ticketoncatalog.service;
import kz.iitu.ticketoncatalog.entity.Duration;
import kz.iitu.ticketoncatalog.entity.Fitness;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.entity.Type;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface FitnessService {
    Fitness createFitness( Fitness fitness);
    List<Fitness> getAllFitness();
    Fitness getFitnessById(Long id);
    Fitness getFitnessByDescription(String description);
    Fitness getFitnessByName(String name);
    List<Fitness> getAllFitnessByType(Type type);
    List<Fitness> getAllFitnessByDuration(Duration duration);
    Map<String, Boolean> deleteFitness(Long id);
    List<Session> findBySessionsIsNotNull();
}
