package kz.iitu.ticketoncatalog.service;
import kz.iitu.ticketoncatalog.entity.Movie;
import kz.iitu.ticketoncatalog.entity.Session;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface MovieService {
    Movie createMovie(Movie movie);
    Movie getMovieById(Long id);
    List<Movie> getAllMovies();
    Movie getMovieByDescription(String description);
    Movie getMovieByName(String name);
    Map<String, Boolean> deleteMovie(Long id);
    List<Session> findBySessionsIsNotNull();
}

