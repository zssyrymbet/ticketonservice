package kz.iitu.ticketoncatalog.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketoncatalog.entity.*;
import kz.iitu.ticketoncatalog.repository.ConcertRepository;
import kz.iitu.ticketoncatalog.service.ConcertService;
import lombok.NonNull;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConcertServiceImpl implements ConcertService {

    @Autowired
    private ConcertRepository concertRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Concert createConcert(Concert concert) {
        concertRepository.saveAndFlush(concert);
        return concert;
    }

    @Override
    public Concert getConcertById(Long id) {
        return concertRepository.getConcertById(id);
    }

    @Override
    public List<Concert> getAllConcerts() {
        return concertRepository.findAll();
    }

    @Override
    public Concert getConcertByLocation(String location) {
        @NonNull Concert concert = concertRepository.getConcertByLocation(location);
        return concert;
    }

    @Override
    public Concert getConcertByName(String name) {
        @NonNull Concert concert = concertRepository.getConcertByName(name);
        return concert;
    }

    @Override
    public Concert getConcertByDescription(String description) {
        @NonNull Concert concert = concertRepository.getConcertByDescription(description);
        return concert;
    }

    @Override
    public Map<String, Boolean> deleteConcert(Long id) {
        concertRepository.findById(id);
        concertRepository.deleteById(id);
        var response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
        fallbackMethod = "getFallbackSession",
        threadPoolKey = "sessionConcertInfo",
        threadPoolProperties = {
            @HystrixProperty(name = "coreSize", value="20"),
            @HystrixProperty(name = "maxQueueSize", value="10")
        }
    )
    public List<Session> findBySessionsIsNotNull() {
        return restTemplate.getForObject("http://session-service/session/concert", ConcertSessions.class).getConcertSessionList();
    }

        public List<Session> getFallbackSession() {
        Session session = new Session();
        session.setId(0l);
        session.setTime("00:00:00");
        Concert concert = new Concert();
        concert.setId(0l);
        concert.setName("unknown");
        concert.setDescription("unknown");
        concert.setLocation("unknown");
        session.setConcert(concert);
        session.setMusical(null);
        session.setMuseum(null);
        session.setMovie(null);
        session.setFitness(null);
        return Arrays.asList(session);
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackRating",
            threadPoolKey = "ratingConcertInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public List<Rating> findByRatingsIsNotNull() {
        return restTemplate.getForObject("http://rating-service/rating/concert", ConcertRatings.class).getConcertRatingList();
    }

    public List<Rating> getFallbackRating() {
        Rating rating = new Rating();
        rating.setId(0l);
        Concert concert = new Concert();
        concert.setId(0l);
        concert.setName("unknown");
        concert.setDescription("unknown");
        concert.setLocation("unknown");
        rating.setConcert(concert);
        rating.setMusical(null);
        rating.setMuseum(null);
        rating.setMovie(null);
        rating.setFitness(null);
        return Arrays.asList(rating);
    }

    //    public List<Session> findBySessionsIsNotNull() {
//        String apiCredentials = "rest-session:p@ssword";
//        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Authorization", "Basic " + base64Credentials);
//        HttpEntity<String> entity = new HttpEntity<>(headers);
//
//        ConcertSessions concertSessions = restTemplate.exchange("http://session-service/session/concert", HttpMethod.GET, entity, ConcertSessions.class).getBody();
//        return concertSessions != null ? concertSessions.getConcertSessionList() : null;
//    }

}
