package kz.iitu.ticketoncatalog.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketoncatalog.entity.*;
import kz.iitu.ticketoncatalog.entity.Duration;
import kz.iitu.ticketoncatalog.entity.Fitness;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.entity.Type;
import kz.iitu.ticketoncatalog.repository.FitnessRepository;
import kz.iitu.ticketoncatalog.service.FitnessService;
import lombok.NonNull;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FitnessServiceImpl implements FitnessService {

    @Autowired
    private FitnessRepository fitnessRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Fitness createFitness(Fitness fitness) {
        return fitnessRepository.saveAndFlush(fitness);
    }

    @Override
    public List<Fitness> getAllFitness() {
        return fitnessRepository.findAll();
    }

    @Override
    public Fitness getFitnessById(Long id){
        return fitnessRepository.getFitnessById(id);
    }

    @Override
    public Fitness getFitnessByDescription(String description) {
        @NonNull Fitness fitness = fitnessRepository.getFitnessByDescription(description);
        return fitness;
    }

    @Override
    public Fitness getFitnessByName(String name) {
        @NonNull Fitness fitness = fitnessRepository.getFitnessByName(name);
        return fitness;
    }

    @Override
    public List<Fitness> getAllFitnessByType(Type type) {
        return (List<Fitness>) fitnessRepository.findFitnessByType(type);
    }

    @Override
    public List<Fitness> getAllFitnessByDuration(Duration duration) {
        return (List<Fitness>) fitnessRepository.findFitnessByDuration(duration);
    }

    @Override
    public Map<String, Boolean> deleteFitness(Long id) {
        fitnessRepository.findById(id);
        fitnessRepository.deleteById(id);
        var response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackSession",
            threadPoolKey = "sessionFitnessInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public List<Session> findBySessionsIsNotNull() {
        String apiCredentials = "rest-session:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        FitnessSessions fitnessSessions = restTemplate.exchange("http://session-service/session/fitness", HttpMethod.GET, entity, FitnessSessions.class).getBody();
        return fitnessSessions != null ? fitnessSessions.getFitnessSessionList() : null;
    }

    public List<Session> getFallbackSession() {
        Session session = new Session();
        session.setId(0l);
        session.setTime("00:00:00");
        Fitness fitness = new Fitness();
        fitness.setId(0l);
        fitness.setName("unknown");
        fitness.setDescription("unknown");
        fitness.setDuration(null);
        fitness.setType(null);
        session.setMusical(null);
        session.setMuseum(null);
        session.setMovie(null);
        session.setConcert(null);
        return Arrays.asList(session);
    }
}
