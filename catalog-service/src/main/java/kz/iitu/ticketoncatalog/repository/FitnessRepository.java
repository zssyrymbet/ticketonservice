package kz.iitu.ticketoncatalog.repository;
import kz.iitu.ticketoncatalog.entity.Duration;
import kz.iitu.ticketoncatalog.entity.Fitness;
import kz.iitu.ticketoncatalog.entity.Session;
import kz.iitu.ticketoncatalog.entity.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface FitnessRepository extends JpaRepository<Fitness, Long> {
    @Query(value = "SELECT fitness FROM Fitness fitness WHERE fitness.name LIKE '%' || :keyword || '%'"
            + " OR fitness.type LIKE '%' || :keyword || '%'"
            + " OR fitness.duration LIKE '%' || :keyword || '%'")
    List<Fitness> searchFitness(@Param("keyword") String keyword);
    Fitness getFitnessByDescription(String description);
    List<Fitness> findFitnessByType(Type type);
    List<Fitness> findFitnessByDuration(Duration duration);
    Fitness getFitnessByName(String name);
    Fitness getFitnessById(Long id);
    List<Session> findBySessionsIsNotNull();
}
