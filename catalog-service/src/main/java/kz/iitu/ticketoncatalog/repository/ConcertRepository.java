package kz.iitu.ticketoncatalog.repository;
import kz.iitu.ticketoncatalog.entity.Concert;
import kz.iitu.ticketoncatalog.entity.Rating;
import kz.iitu.ticketoncatalog.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ConcertRepository extends JpaRepository<Concert, Long> {
    @Query(value = "SELECT concert FROM Concert concert WHERE concert.name LIKE '%' || :keyword || '%'"
            + " OR concert.location LIKE '%' || :keyword || '%'"
            + " OR concert.description LIKE '%' || :keyword || '%'")
    List<Concert> searchConcert(@Param("keyword") String keyword);
    Concert getConcertByLocation(String location);
    Concert getConcertByDescription(String description);
    Concert getConcertByName(String name);
    Concert getConcertById(Long id);
    List<Session> findBySessionsIsNotNull();
    List<Rating> findByRatingsIsNotNull();
}

