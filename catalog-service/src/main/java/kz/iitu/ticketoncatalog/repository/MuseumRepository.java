package kz.iitu.ticketoncatalog.repository;
import kz.iitu.ticketoncatalog.entity.Museum;
import kz.iitu.ticketoncatalog.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MuseumRepository extends JpaRepository<Museum, Long> {
    @Query(value = "SELECT museum FROM Museum museum WHERE museum.name LIKE '%' || :keyword || '%'"
            + " OR museum.description LIKE '%' || :keyword || '%'")
    List<Museum> searchMuseum(@Param("keyword") String keyword);
    Museum getMuseumByDescription(String description);
    Museum getMuseumByName(String name);
    Museum getMuseumById(Long id);
    List<Session> findBySessionsIsNotNull();
}
