package kz.iitu.ticketoncatalog.repository;
import kz.iitu.ticketoncatalog.entity.Musical;
import kz.iitu.ticketoncatalog.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MusicalRepository extends JpaRepository<Musical, Long> {
    @Query(value = "SELECT musical FROM Musical musical WHERE musical.name LIKE '%' || :keyword || '%'"
            + " OR musical.description LIKE '%' || :keyword || '%'")
    List<Musical> searchMusical(@Param("keyword") String keyword);
    Musical getMusicalByDescription(String description);
    Musical getMusicalByName(String name);
    Musical getMusicalById(Long id);
    List<Session> findBySessionsIsNotNull();
}
