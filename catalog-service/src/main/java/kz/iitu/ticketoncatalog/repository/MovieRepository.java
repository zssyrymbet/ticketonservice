package kz.iitu.ticketoncatalog.repository;
import kz.iitu.ticketoncatalog.entity.Movie;
import kz.iitu.ticketoncatalog.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    @Query(value = "SELECT movie FROM Movie movie WHERE movie.name LIKE '%' || :keyword || '%'"
            + " OR movie.description LIKE '%' || :keyword || '%'"
            + " OR movie.county LIKE '%' || :keyword || '%'"
            + " OR movie.year LIKE '%' || :keyword || '%'")
    List<Movie> searchMovie(@Param("keyword") String keyword);
    Movie getMovieByDescription(String description);
    Movie getMovieByName(String name);
    Movie getMovieById(Long id);
    List<Session> findBySessionsIsNotNull();
}
