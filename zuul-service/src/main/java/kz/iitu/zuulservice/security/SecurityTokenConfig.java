package kz.iitu.zuulservice.security;
import javax.servlet.http.HttpServletResponse;
import kz.iitu.commonservice.security.JwtConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtConfig jwtConfig;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and()
                .addFilterAfter(new JwtTokenAuthenticationFilter(jwtConfig), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, jwtConfig.getUri()).permitAll()
                .antMatchers("/client/client**").hasAuthority("ROLE_CLIENT")
                .antMatchers("/client/user/**").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/api/concert/**").permitAll()
                .antMatchers("/api/fitness/**").permitAll()
                .antMatchers("/api/movie/**").permitAll()
                .antMatchers("/api/museum/**").permitAll()
                .antMatchers("/api/musical/**").permitAll()
                .antMatchers("/session/session/**").permitAll()
                .antMatchers("/ticket/ticket/booking/**").hasAuthority("ROLE_CLIENT")
//                .antMatchers("/ticket/ticket/**").permitAll()
                .antMatchers("/ticket/place/**").permitAll()
                .antMatchers("/booking/booking/**").hasAuthority("ROLE_CLIENT")
                .anyRequest().authenticated();
    }

    @Bean
    public JwtConfig jwtConfig() {
        return new JwtConfig();
    }
}
