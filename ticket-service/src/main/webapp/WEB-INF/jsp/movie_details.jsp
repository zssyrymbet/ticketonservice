<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Movie Details</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/jumbotron/">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .btn-outline-success {
        margin-right: 10px !important;
      }

      .card {
        margin-right: 25px;
        margin-bottom: 20px;
      }

      .btn-secondary {
        border-color: #00b3b3;
        background-color: #00b3b3;
        margin-top: 10px;
      }
      h1 {
        margin-top: 20px;
      }
      div.col-md-10.col-md-offset-1 {
        margin-top: 70px !important;
      }
    </style>
    <link href="jumbotron.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/index">Ticketon</a>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="/concert_page">Concert</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/fitness_page">Fitness</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/movie_page">Movie</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/museum_page">Museum</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/musical_page">Musical</a>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                 <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                 <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                 <a class="btn btn-outline-success my-2 my-sm-0" href="/signin" role="button">Sign In</a>
            </form>
        </div>
    </nav>
    <main role="main">
      <header class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <h1>Movie: ${session.movie.name}</h1>
          </div>
        </div>
      </header>

      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <figure class="text-center">
                 <img src="https://picsum.photos/300/400" alt="The Amazing Product" class="img-thumbnail" />
                 <p><a class="btn btn-secondary" href="/booking_form?id=${session.id}" role="button">Book</a></p>
            </figure>
          </div>
          <div class="col-xs-12 col-md-4 col-md-offset-1">
            <p>
                Country: ${session.movie.county}
            </p>
            <p>
                Year: ${session.movie.year}
            </p>
             <p>
                Description: ${session.movie.description}
             </p>
          </div>
        </div>
    </div>
  </main>
</body>
</html>
