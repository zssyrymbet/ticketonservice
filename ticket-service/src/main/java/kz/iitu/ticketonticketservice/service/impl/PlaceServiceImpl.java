package kz.iitu.ticketonticketservice.service.impl;
import kz.iitu.ticketonticketservice.entity.*;
import kz.iitu.ticketonticketservice.repository.PlaceRepository;
import kz.iitu.ticketonticketservice.service.PlaceService;
import lombok.NonNull;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PlaceServiceImpl implements PlaceService {

    @Autowired
    private PlaceRepository placeRepository;

    @Override
    public Place createPlace(Place place) {
        placeRepository.saveAndFlush(place);
        return place;
    }

    @Override
    public List<Place> getAllPlaces() {
        return placeRepository.findAll();
    }

    @Override
    public Place getPlaceById(Long id) {
        return placeRepository.getPlaceById(id);
    }

    @Override
    public Place getPlaceByName(String name) {
        @NonNull Place place = placeRepository.getPlaceByName(name);
        return place;
    }

    @Override
    public Place getPlaceByTicket(Ticket ticket) {
        @NonNull Place place = placeRepository.getPlaceByTicket(ticket);
        return place;
    }

    @Override
    public Map<String, Boolean> deletePlace(Long id) {
        placeRepository.findById(id);
        placeRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
