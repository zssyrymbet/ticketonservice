package kz.iitu.ticketonticketservice.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketonticketservice.entity.*;
import kz.iitu.ticketonticketservice.repository.TicketRepository;
import kz.iitu.ticketonticketservice.service.TicketService;
import lombok.NonNull;
import lombok.val;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Ticket createTicket(Ticket ticket) {
        ticketRepository.saveAndFlush(ticket);
        return ticket;
    }

    @Override
    public Ticket getTicketById(Long id){
        return ticketRepository.getTicketById(id);
    }

    @Override
    public List<Ticket> getAllTickets() {
        return ticketRepository.findAll();
    }

    @Override
    public Ticket findTicketBySessionId(Long id) {
        return ticketRepository.findTicketBySessionId(id);
    }

    @Override
    public Ticket findTicketByPlace(String name) {
        @NonNull Ticket ticket = ticketRepository.findTicketByPlace(name);
        return ticket;
    }

    @Override
    public Map<String, Boolean> deleteTicket(Long id){
        ticketRepository.findById(id);
        ticketRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackBooking",
            threadPoolKey = "ticketBookingInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public Booking getBookingById(Long ticket_id) {
        String apiCredentials = "rest-booking:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange("http://booking-service/booking/ticket/", HttpMethod.GET, entity, Booking.class).getBody();
    }

    public Booking getFallbackBooking(Long ticket_id) {
        Booking booking = new Booking();
        booking.setId(0l);
        booking.setClient(null);
        booking.setQuantity(0);
        booking.setTicket(null);
        return booking;
    }
}
