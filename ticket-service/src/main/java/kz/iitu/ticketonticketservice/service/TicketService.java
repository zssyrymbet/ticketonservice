package kz.iitu.ticketonticketservice.service;
import kz.iitu.ticketonticketservice.entity.*;
import java.util.List;
import java.util.Map;

public interface TicketService {
    Ticket createTicket(Ticket ticket);
    Ticket getTicketById(Long id);
    List<Ticket> getAllTickets();
    Ticket findTicketBySessionId(Long id);
    Ticket findTicketByPlace(String name);
    Map<String, Boolean> deleteTicket(Long id);
    Booking getBookingById(Long ticket_id);
}
