package kz.iitu.ticketonticketservice.service;
import kz.iitu.ticketonticketservice.entity.*;
import java.util.List;
import java.util.Map;

public interface PlaceService {
    Place createPlace(Place place);
    List<Place> getAllPlaces();
    Place getPlaceById(Long id);
    Place getPlaceByName(String name);
    Place getPlaceByTicket(Ticket ticket);
    Map<String, Boolean> deletePlace(Long id);
}
