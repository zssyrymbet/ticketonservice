package kz.iitu.ticketonticketservice.entity;

public enum Type {
    MALE,
    FEMALE,
    STUDENT,
}
