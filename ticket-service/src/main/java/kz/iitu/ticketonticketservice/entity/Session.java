package kz.iitu.ticketonticketservice.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "sessions")
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String time;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "concert_id", updatable = false)
    @JsonIgnoreProperties("sessions")
    private Concert concert;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "movie_id", updatable = false)
    @JsonIgnoreProperties("sessions")
    private Movie movie;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "musical_id", updatable = false)
    @JsonIgnoreProperties("sessions")
    private Musical musical;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "museum_id", updatable = false)
    @JsonIgnoreProperties("sessions")
    private Museum museum;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "fitness_id", updatable = false)
    @JsonIgnoreProperties("fitness")
    private Fitness fitness;

    @OneToMany(mappedBy = "session", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Ticket> tickets;

    @JsonIgnore
    public Set<Ticket> getTickets() {
        return tickets;
    }

    @JsonProperty
    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }
}
