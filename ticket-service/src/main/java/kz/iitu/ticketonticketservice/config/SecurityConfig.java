package kz.iitu.ticketonticketservice.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
//                .antMatchers("**/admin/delete/**").hasRole("ADMIN")
//                .antMatchers("**/admin/create/**").hasRole("ADMIN")
                .antMatchers("/place/**").permitAll()
                .antMatchers("/ticket/admin/create").permitAll()
                .antMatchers("/ticket/all").permitAll()
                .antMatchers("/ticket/booking/**").permitAll()
                .and()
                .httpBasic();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password("{noop}p@ssword")
                .roles("ADMIN");
        auth.inMemoryAuthentication()
                .withUser("client")
                .password("{noop}p@ssword")
                .roles("CLIENT");
    }
}
