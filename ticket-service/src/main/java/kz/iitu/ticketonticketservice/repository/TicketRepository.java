package kz.iitu.ticketonticketservice.repository;
import kz.iitu.ticketonticketservice.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Ticket findTicketBySessionId(Long id);
    Ticket findTicketByPlace(String name);
    Ticket getTicketById(Long id);
    Booking getBookingById(Long ticket_id);
}
