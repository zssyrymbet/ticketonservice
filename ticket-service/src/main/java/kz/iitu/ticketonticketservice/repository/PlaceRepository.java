package kz.iitu.ticketonticketservice.repository;
import kz.iitu.ticketonticketservice.entity.Place;
import kz.iitu.ticketonticketservice.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceRepository extends JpaRepository<Place, Long> {
    Place getPlaceByTicket(Ticket ticket);
    Place getPlaceByName(String name);
    Place getPlaceById(Long id);
}
