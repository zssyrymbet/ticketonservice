package kz.iitu.ticketonticketservice.controller;
import kz.iitu.ticketonticketservice.entity.*;
import kz.iitu.ticketonticketservice.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/place")
public class PlaceController {

    @Autowired
    private PlaceService placeService;

    @PostMapping("/admin/create")
    public Place createPlace(@RequestBody Place place)  {
        return placeService.createPlace(place);
    }

    @GetMapping("/id/{id}")
    public Place getPlaceById(@PathVariable(value = "id") Long id) {
        return placeService.getPlaceById(id);
    }

    @GetMapping("/name/{name}")
    public Place getPlaceByName(@PathVariable(value = "name") String name) {
        return placeService.getPlaceByName(name);
    }

    @GetMapping("/all")
    public List<Place> getAllPlaces() {
        System.out.println(placeService.getAllPlaces());
        return placeService.getAllPlaces();
    }

    @GetMapping("/tickets")
    public Place getPlaceByTicket(@RequestBody Ticket ticket) {
        return placeService.getPlaceByTicket(ticket);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deletePlace(@PathVariable(value = "id") Long id) {
        return placeService.deletePlace(id);
    }
}
