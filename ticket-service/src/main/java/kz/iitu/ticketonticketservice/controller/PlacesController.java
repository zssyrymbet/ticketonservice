package kz.iitu.ticketonticketservice.controller;
import kz.iitu.ticketonticketservice.entity.Place;
import kz.iitu.ticketonticketservice.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class PlacesController {

    @Autowired
    private PlaceService placeService;

    @RequestMapping(value = "/save_place", method = RequestMethod.POST)
    public String createMusical(@ModelAttribute("place") Place place) {
        placeService.createPlace(place);
        return "redirect:/places";
    }

    @RequestMapping("/place_form")
    public String showPlaceForm(Map<String, Object> model) {
        Place place = new Place();
        model.put("place", place);
        return "new_place";
    }

    @RequestMapping("/delete_place")
    public String delPlace(@RequestParam Long id) {
        placeService.deletePlace(id);
        return "redirect:/places";
    }

    @RequestMapping(value = { "/places" }, method = RequestMethod.GET)
    public String getAllPlaces(Model model) {
        List<Place> places = placeService.getAllPlaces();
        System.out.println("place" + placeService.getAllPlaces());
        model.addAttribute("places", places);
        return "places";
    }
}
