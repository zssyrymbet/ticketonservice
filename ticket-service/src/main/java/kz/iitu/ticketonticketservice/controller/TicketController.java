package kz.iitu.ticketonticketservice.controller;
import kz.iitu.ticketonticketservice.entity.*;
import kz.iitu.ticketonticketservice.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @PostMapping("/admin/create")
    public void createTicket(@RequestBody Ticket ticket)  {
        ticketService.createTicket(ticket);
    }

    @GetMapping("/place/{name}")
    public Ticket findTicketByPlace(@PathVariable(value = "name") String name) {
        return ticketService.findTicketByPlace(name);
    }

    @GetMapping("/booking/{ticket_id}")
    public Booking getBookingById(@PathVariable(value = "ticket_id") Long ticket_id) {
        return ticketService.getBookingById(ticket_id);
    }

    @GetMapping("/id/{id}")
    public Ticket getTicketById(@PathVariable(value = "id") Long id) {
        return ticketService.getTicketById(id);
    }

    @GetMapping("/all")
    public List<Ticket> getAllTickets() {
        return ticketService.getAllTickets();
    }

    @GetMapping("/session/{id}")
    public Ticket findTicketBySessionId(@PathVariable(value = "id") Long id) {
        return ticketService.findTicketBySessionId(id);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteTicket(@PathVariable(value = "id") Long id) {
        return ticketService.deleteTicket(id);
    }
}
