package kz.iitu.ticketonticketservice.controller;
import kz.iitu.ticketonticketservice.entity.Booking;
import kz.iitu.ticketonticketservice.entity.Ticket;
import kz.iitu.ticketonticketservice.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class TicketsController {

    @Autowired
    private TicketService ticketService;

    @RequestMapping(value = "/save_ticket", method = RequestMethod.POST)
    public String createTicket(@ModelAttribute("ticket") Ticket ticket) {
        ticketService.createTicket(ticket);
        return "redirect:/tickets";
    }

    @RequestMapping("/ticket_form")
    public String showTicketForm(Map<String, Object> model) {
        Ticket ticket = new Ticket();
        model.put("ticket", ticket);
        return "new_ticket";
    }

    @RequestMapping("/delete_ticket")
    public String delTicket(@RequestParam Long id) {
        ticketService.deleteTicket(id);
        return "redirect:/tickets";
    }

    @RequestMapping("/booking_form")
    public String getTicketSession(@RequestParam Long id, Map<String, Object> model) {
        Ticket ticket = ticketService.findTicketBySessionId(id);
        Booking booking = new Booking();
        model.put("ticket", ticket);
        model.put("booking", booking);
        return "book";
    }

    @RequestMapping(value = { "/tickets" }, method = RequestMethod.GET)
    public String getAllTickets(Model model) {
        List<Ticket> tickets = ticketService.getAllTickets();
        model.addAttribute("tickets", tickets);
        return "tickets";
    }
}
