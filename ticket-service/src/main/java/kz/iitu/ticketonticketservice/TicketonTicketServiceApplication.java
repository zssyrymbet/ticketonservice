package kz.iitu.ticketonticketservice;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@EnableHystrixDashboard
@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class TicketonTicketServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketonTicketServiceApplication.class, args);
	}

}
