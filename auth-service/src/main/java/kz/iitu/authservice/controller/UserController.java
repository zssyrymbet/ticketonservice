package kz.iitu.authservice.controller;
import kz.iitu.authservice.entity.User;
import kz.iitu.authservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user/registration", method = RequestMethod.POST)
    public String registration(@RequestBody User userForm, BindingResult bindingResult)  {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.createUser(userForm);
        return "redirect:/welcome";
    }

    public String welcome(Model model) {
        return "welcome";
    }

    @GetMapping("/api/users")
    public List<User> getAllUsers() {
        System.out.println(userService.getAllUsers());
        return userService.getAllUsers();
    }


    @GetMapping("/users/id/{id}")
    public User findById(Long id) {
        return userService.findById(id);
    }

    @GetMapping("/users/username/{username}")
    public User findByUsername(String username) {
        return userService.findByUsername(username);
    }

    @DeleteMapping("/admin/delete/users/{id}")
    public Map<String, Boolean> deleteUser(Long id) {
        return userService.deleteUser(id);
    }

}


