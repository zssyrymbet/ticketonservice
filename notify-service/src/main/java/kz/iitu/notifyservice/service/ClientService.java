package kz.iitu.notifyservice.service;
//import kz.iitu.notifyservice.entity.BookingRequest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.io.IOException;

@Service
public class ClientService {
    @KafkaListener(topics = "booking_requests", groupId = "group_id")
    public void consume(String bookingRequest) throws IOException {
        System.out.println(String.format("---------- Notify client by email: -> %s",
                "Client with id booked the ticket " + bookingRequest));
    }
}
