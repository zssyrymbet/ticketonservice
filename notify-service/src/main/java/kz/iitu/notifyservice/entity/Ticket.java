package kz.iitu.notifyservice.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "tickets")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "session_id", updatable = false)
    @JsonIgnoreProperties("tickets")
    private Session session;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place_id")
    private Place place;

    private Double price;

    private int quantity;

    @OneToMany(mappedBy = "ticket", fetch = FetchType.LAZY)
    private Set<Booking> bookings;

    @JsonIgnore
    public Set<Booking> getBookings() {
        return bookings;
    }

    @JsonProperty
    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }
}
