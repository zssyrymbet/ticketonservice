package kz.iitu.notifyservice.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "fitness")
public class Fitness {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Enumerated(EnumType.STRING)
    private Duration duration;

    @OneToMany(mappedBy = "fitness", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Session> sessions;

    @JsonIgnore
    public Set<Session> getSessions() {
        return sessions;
    }

    @JsonProperty
    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }
}