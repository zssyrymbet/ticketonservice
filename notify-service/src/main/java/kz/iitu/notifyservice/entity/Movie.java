package kz.iitu.notifyservice.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private int year;

    private String county;

    private String description;

    @OneToMany(mappedBy = "movie", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Session> sessions;

    @JsonIgnore
    public Set<Session> getSessions() {
        return sessions;
    }

    @JsonProperty
    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }
}
