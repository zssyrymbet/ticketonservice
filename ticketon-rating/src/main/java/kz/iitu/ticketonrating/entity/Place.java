package kz.iitu.ticketonrating.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "places")
public class Place {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String location;

    private int seat;

    @OneToOne(mappedBy = "place", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Ticket ticket;

    @JsonIgnore
    public Ticket getTicket() {
        return ticket;
    }

    @JsonProperty
    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
