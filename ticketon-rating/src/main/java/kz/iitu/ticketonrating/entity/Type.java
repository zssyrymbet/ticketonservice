package kz.iitu.ticketonrating.entity;

public enum Type {
    MALE,
    FEMALE,
    STUDENT,
}
