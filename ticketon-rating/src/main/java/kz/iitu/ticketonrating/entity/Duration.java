package kz.iitu.ticketonrating.entity;

public enum Duration {
    TWO_TIMES_PER_WEEK,
    THREE_TIMES_PER_WEEK,
    FOUR_TIMES_PER_WEEK,
    MONTHLY_UNLIMITED,
}

