package kz.iitu.ticketonrating.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MuseumRatings {

    private List<Rating> museumRatingList;

    public MuseumRatings() {}

    public MuseumRatings(List<Rating> museumRatingList) {
        this.museumRatingList = museumRatingList;
    }
}
