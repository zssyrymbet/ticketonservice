package kz.iitu.ticketonrating.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ratings")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int rating;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "concert_id", updatable = false)
    @JsonIgnoreProperties("ratings")
    private Concert concert;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "movie_id", updatable = false)
    @JsonIgnoreProperties("ratings")
    private Movie movie;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "musical_id", updatable = false)
    @JsonIgnoreProperties("ratings")
    private Musical musical;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "museum_id", updatable = false)
    @JsonIgnoreProperties("ratings")
    private Museum museum;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "fitness_id", updatable = false)
    @JsonIgnoreProperties("ratings")
    private Fitness fitness;
}
