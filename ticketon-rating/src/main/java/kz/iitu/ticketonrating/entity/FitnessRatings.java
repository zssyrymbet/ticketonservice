package kz.iitu.ticketonrating.entity;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class FitnessRatings {

    private List<Rating> fitnessRatingList;

    public FitnessRatings() {}

    public FitnessRatings(List<Rating> fitnessRatingList) {
        this.fitnessRatingList = fitnessRatingList;
    }
}
