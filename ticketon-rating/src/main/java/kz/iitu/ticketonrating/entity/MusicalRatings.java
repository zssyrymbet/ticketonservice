package kz.iitu.ticketonrating.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MusicalRatings {

    private List<Rating> musicalRatingList;

    public MusicalRatings() {}

    public MusicalRatings(List<Rating> musicalRatingList) {
        this.musicalRatingList = musicalRatingList;
    }
}
