package kz.iitu.ticketonrating.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MovieRatings {

    private List<Rating> movieRatingList;

    public MovieRatings() {}

    public MovieRatings(List<Rating> movieRatingList) {
        this.movieRatingList = movieRatingList;
    }
}
