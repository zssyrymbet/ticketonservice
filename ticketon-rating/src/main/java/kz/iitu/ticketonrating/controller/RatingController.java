package kz.iitu.ticketonrating.controller;
import kz.iitu.ticketonrating.entity.*;
import kz.iitu.ticketonrating.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rating")
public class RatingController {

    @Autowired
    private RatingService ratingService;

    @PostMapping("/admin/create")
    public void createSession(@RequestBody Rating rating)  {
        ratingService.createRating(rating);
    }

    @GetMapping("/id/{id}")
    public Rating getRatingById(@PathVariable(value = "id") Long id) {
        return ratingService.getRatingById(id);
    }

    @GetMapping("/all")
    public List<Rating> getAllRatings() {
        return ratingService.getAllRating();
    }

    @GetMapping("/concert")
    public ConcertRatings findByConcertIsNotNul() {
        return ratingService.findByConcertIsNotNull();
    }

    @GetMapping("/fitness")
    public FitnessRatings findSessionByFitness() {
        return ratingService.findByFitnessIsNotNull();
    }

    @GetMapping("/museum")
    public MuseumRatings findSessionByMuseum() {
        return ratingService.findByMuseumIsNotNull();
    }

    @GetMapping("/musical")
    public MusicalRatings findSessionByMusical() {
        return ratingService.findByMusicalIsNotNull();
    }

    @GetMapping("/movie")
    public MovieRatings findSessionByMovie() {
        return ratingService.findByMovieIsNotNull();
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteRating(@PathVariable(value = "id") Long id) {
        return ratingService.deleteRating(id);
    }
}
