package kz.iitu.ticketonrating.repository;
import kz.iitu.ticketonrating.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {
    List<Rating> findByConcertIsNotNull();
    List<Rating> findByMovieIsNotNull();
    List<Rating> findByMusicalIsNotNull();
    List<Rating> findByMuseumIsNotNull();
    List<Rating> findByFitnessIsNotNull();
    Rating getRatingById(Long id);

}

