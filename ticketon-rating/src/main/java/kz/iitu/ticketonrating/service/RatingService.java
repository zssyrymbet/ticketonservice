package kz.iitu.ticketonrating.service;
import kz.iitu.ticketonrating.entity.*;

import java.util.List;
import java.util.Map;

public interface RatingService {
    Rating createRating(Rating rating);
    List<Rating> getAllRating();
    Rating getRatingById(Long id);
    ConcertRatings findByConcertIsNotNull();
    MovieRatings findByMovieIsNotNull();
    MusicalRatings findByMusicalIsNotNull();
    MuseumRatings findByMuseumIsNotNull();
    FitnessRatings findByFitnessIsNotNull();
    Map<String, Boolean> deleteRating(Long id);
}
