package kz.iitu.ticketonrating.service.impl;

import kz.iitu.ticketonrating.entity.*;
import kz.iitu.ticketonrating.repository.RatingRepository;
import kz.iitu.ticketonrating.service.RatingService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RatingServiceImpl implements RatingService {
    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Rating createRating(Rating rating) {
        ratingRepository.saveAndFlush(rating);
        return rating;
    }

    @Override
    public List<Rating> getAllRating() {
        return ratingRepository.findAll();
    }

    @Override
    public Rating getRatingById(Long id) {
        return ratingRepository.getRatingById(id);
    }

    @Override
    public ConcertRatings findByConcertIsNotNull() {
        List<Rating> ratings = ratingRepository.findByConcertIsNotNull();
        ConcertRatings concertRatings = new ConcertRatings();
        concertRatings.setConcertRatingList(ratings);
        return concertRatings;
    }

    @Override
    public MovieRatings findByMovieIsNotNull() {
        List<Rating> ratings = ratingRepository.findByMovieIsNotNull();
        MovieRatings movieRatings = new MovieRatings();
        movieRatings.setMovieRatingList(ratings);
        return movieRatings;
    }

    @Override
    public MuseumRatings findByMuseumIsNotNull() {
        List<Rating> ratings = ratingRepository.findByMuseumIsNotNull();
        MuseumRatings museumRatings = new MuseumRatings();
        museumRatings.setMuseumRatingList(ratings);
        return museumRatings;
    }

    @Override
    public MusicalRatings findByMusicalIsNotNull() {
        List<Rating> ratings = ratingRepository.findByMusicalIsNotNull();
        MusicalRatings musicalRatings = new MusicalRatings();
        musicalRatings.setMusicalRatingList(ratings);
        return musicalRatings;
    }

    @Override
    public FitnessRatings findByFitnessIsNotNull() {
        List<Rating> ratings = ratingRepository.findByFitnessIsNotNull();
        FitnessRatings fitnessRatings = new FitnessRatings();
        fitnessRatings.setFitnessRatingList(ratings);
        return fitnessRatings;
    }

    @Override
    public Map<String, Boolean> deleteRating(Long id) {
        ratingRepository.findById(id);
        ratingRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}