**Getting started**
git clone git@gitlab.com:zssyrymbet/ticketonservice.git
cd ticketon
run project

**Project description**
In applications, the administrator can add all the data to the table, create and view existing tables, get certain fields with data from the database. The client can register and go to the site, view session and ticket details and book tickets, view booking history.

**How to work project?**
The ticket catalog service will provide us with the opportunity to watch and select entertainment programs. We will contact the information service to obtain certain data that relate to cinema or theater. Using the session service, we can see if there are available sessions for this cinema or theater. Then using the ticket services, we can find tickets for this session and using the booking service we can book a ticket. In the service of users, the client can register and go to the site, after booking the data is saved in the client's history.

**Microservices**

> 1.Ticket catalog service port=8080
This microservice will store all data about different types of entertainment areas (cinema, theater, fitness, museum, concert) that this site will provide to customers.


> 2.Session service        port=8081
In this service, we will create different sessions and show the available sessions on the site, which will contain information about the time, tickets for a sensation, an ID of a concert or a movie. 


> 3.Ticketing services     port=8082
In the ticket service, we will create tickets for various entertainment areas, we will indicate its price, the place where it will take place, the session number, the number of tickets for this session.


> 4.User service           port=8083
On our site there will be two types of user - client and admin.
Using this service, the client can register on the site, go to the site, see his history, the admin can create different sessions, delete or change some data.


> 5.Booking service        port=8084
After a person has looked at the information, he can book it for this, we will create a service that will be responsible for the logic of the booking.
