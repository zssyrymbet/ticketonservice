package kz.iitu.ticketonsessionservice.controller;
import kz.iitu.ticketonsessionservice.entity.*;
import kz.iitu.ticketonsessionservice.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/session")
public class SessionController {

    @Autowired
    private SessionService sessionService;

    @PostMapping("/admin/create")
    public void createSession(@RequestBody Session session)  {
        sessionService.createSession(session);
    }

    @GetMapping("/id/{id}")
    public Session getSessionById(@PathVariable(value = "id") Long id) {
        return sessionService.getSessionById(id);
    }

    @GetMapping("/ticket/{id}")
    public Ticket getTicketById(@PathVariable(value = "id") Long id) {
        return sessionService.getTicketById(id);
    }

    @GetMapping("/all")
    public List<Session> getAllSessions() {
        return sessionService.getAllSessions();
    }

    @GetMapping("/concert")
    public ConcertSessions findByConcertIsNotNul() {
        return sessionService.findByConcertIsNotNull();
    }

    @GetMapping("/fitness")
    public FitnessSessions findSessionByFitness() {
        return sessionService.findByFitnessIsNotNull();
    }

    @GetMapping("/museum")
    public MuseumSessions findSessionByMuseum() {
        return sessionService.findByMuseumIsNotNull();
    }

    @GetMapping("/musical")
    public MusicalSessions findSessionByMusical() {
        return sessionService.findByMusicalIsNotNull();
    }

    @GetMapping("/movie")
    public MovieSessions findSessionByMovie() {
        return sessionService.findByMovieIsNotNull();
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteSession(@PathVariable(value = "id") Long id) {
        return sessionService.deleteSession(id);
    }
}
