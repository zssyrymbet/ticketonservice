package kz.iitu.ticketonsessionservice.controller;
import kz.iitu.ticketonsessionservice.entity.*;
import kz.iitu.ticketonsessionservice.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class SessionsController {

    @Autowired
    private SessionService sessionService;

    @RequestMapping(value = "/save_session", method = RequestMethod.POST)
    public String createSession(@ModelAttribute("session") Session session) {
        sessionService.createSession(session);
        return "redirect:/sessions";
    }

    @RequestMapping("/session_form")
    public String showSessionForm(Map<String, Object> model) {
        Session session = new Session();
        model.put("session", session);
        return "new_session";
    }

    @RequestMapping("/delete_session")
    public String delSession(@RequestParam Long id) {
        sessionService.deleteSession(id);
        return "redirect:/sessions";
    }

    @RequestMapping(value = { "/sessions" }, method = RequestMethod.GET)
    public String getAllSessions(Model model) {
        List<Session> sessions = sessionService.getAllSessions();
        model.addAttribute("sessions", sessions);
        return "sessions";
    }

    @RequestMapping(value = { "/concert_page" }, method = RequestMethod.GET)
    public String getSessionByConcert(Model model) {
        ConcertSessions sessions = sessionService.findByConcertIsNotNull();
        model.addAttribute("sessions", sessions);
        return "concert_page";
    }

    @RequestMapping(value = { "/fitness_page" }, method = RequestMethod.GET)
    public String getSessionByFitness(Model model) {
        FitnessSessions sessions = sessionService.findByFitnessIsNotNull();
        model.addAttribute("sessions", sessions);
        return "fitness_page";
    }

    @RequestMapping(value = { "/movie_page" }, method = RequestMethod.GET)
    public String getSessionByMovie(Model model) {
        MovieSessions sessions = sessionService.findByMovieIsNotNull();
        model.addAttribute("sessions", sessions);
        return "movie_page";
    }

    @RequestMapping(value = { "/museum_page" }, method = RequestMethod.GET)
    public String getSessionByMuseum(Model model) {
        MuseumSessions sessions = sessionService.findByMuseumIsNotNull();
        model.addAttribute("sessions", sessions);
        return "museum_page";
    }

    @RequestMapping(value = { "/musical_page" }, method = RequestMethod.GET)
    public String getSessionByMusical(Model model) {
        MusicalSessions sessions = sessionService.findByMusicalIsNotNull();
        model.addAttribute("sessions", sessions);
        return "musical_page";
    }

    @RequestMapping("/concert_details")
    public String getTicketBySession(@RequestParam Long id, Model model) {
        Session session = sessionService.getSessionById(id);
        model.addAttribute("session", session);
        return "concert_details";
    }

    @RequestMapping("/fitness_details")
    public String getFitnessById(@RequestParam Long id, Model model) {
        Session session = sessionService.getSessionById(id);
        model.addAttribute("session", session);
        return "fitness_details";
    }

    @RequestMapping("/movie_details")
    public String getMovieById(@RequestParam Long id, Model model) {
        Session session = sessionService.getSessionById(id);
        model.addAttribute("session", session);
        return "movie_details";
    }

    @RequestMapping("/museum_details")
    public String getMuseumById(@RequestParam Long id, Model model) {
        Session session = sessionService.getSessionById(id);
        model.addAttribute("session", session);
        return "museum_details";
    }

    @RequestMapping("/musical_details")
    public String getMusicalById(@RequestParam Long id, Model model) {
        Session session = sessionService.getSessionById(id);
        model.addAttribute("session", session);
        return "musical_details";
    }



}
