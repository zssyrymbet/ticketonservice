package kz.iitu.ticketonsessionservice.entity;

public enum Type {
    MALE,
    FEMALE,
    STUDENT,
}
