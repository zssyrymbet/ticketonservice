package kz.iitu.ticketonsessionservice.entity;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class ConcertSessions {

    private List<Session> concertSessionList;

    public ConcertSessions() {}

    public ConcertSessions(List<Session> concertSessionList) {
        this.concertSessionList = concertSessionList;
    }
}
