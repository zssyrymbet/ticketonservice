package kz.iitu.ticketonsessionservice.entity;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class MuseumSessions {

    private List<Session> museumSessionList;

    public MuseumSessions() {}

    public MuseumSessions(List<Session> museumSessionList) {
        this.museumSessionList = museumSessionList;
    }
}
