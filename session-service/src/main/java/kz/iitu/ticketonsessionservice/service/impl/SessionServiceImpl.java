package kz.iitu.ticketonsessionservice.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketonsessionservice.entity.*;
import kz.iitu.ticketonsessionservice.repository.SessionRepository;
import kz.iitu.ticketonsessionservice.service.SessionService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.val;

@Service
public class SessionServiceImpl implements SessionService {
    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Session createSession(Session session) {
        sessionRepository.saveAndFlush(session);
        return session;
    }

    @Override
    public List<Session> getAllSessions() {
        return sessionRepository.findAll();
    }

    @Override
    public Session getSessionById(Long id) {
        return sessionRepository.getSessionById(id);
    }

    @Override
    public ConcertSessions findByConcertIsNotNull() {
        List<Session> sessions = sessionRepository.findByConcertIsNotNull();
        ConcertSessions concertSessions = new ConcertSessions();
        concertSessions.setConcertSessionList(sessions);
        return concertSessions;
    }

    @Override
    public MovieSessions findByMovieIsNotNull() {
        List<Session> sessions = sessionRepository.findByMovieIsNotNull();
        MovieSessions movieSessions = new MovieSessions();
        movieSessions.setMovieSessionList(sessions);
        return movieSessions;
    }

    @Override
    public MuseumSessions findByMuseumIsNotNull() {
        List<Session> sessions = sessionRepository.findByMuseumIsNotNull();
        MuseumSessions museumSessions = new MuseumSessions();
        museumSessions.setMuseumSessionList(sessions);
        return museumSessions;
    }

    @Override
    public MusicalSessions findByMusicalIsNotNull() {
        List<Session> sessions = sessionRepository.findByMusicalIsNotNull();
        MusicalSessions musicalSessions = new MusicalSessions();
        musicalSessions.setMusicalSessionList(sessions);
        return musicalSessions;
    }

    @Override
    public FitnessSessions findByFitnessIsNotNull() {
        List<Session> sessions = sessionRepository.findByFitnessIsNotNull();
        FitnessSessions fitnessSessions = new FitnessSessions();
        fitnessSessions.setFitnessSessionList(sessions);
        return fitnessSessions;
    }

    @Override
    public Map<String, Boolean> deleteSession(Long id) {
        sessionRepository.findById(id);
        sessionRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackTicket",
            threadPoolKey = "sessionTicketInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public Ticket getTicketById(Long id) {
        String apiCredentials = "rest-ticket:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange("http://ticket-service/ticket/session/" + id, HttpMethod.GET, entity, Ticket.class).getBody();
    }

    public Ticket getFallbackTicket() {
        Ticket ticket = new Ticket();
        ticket.setId(0l);
        ticket.setPlace(null);
        ticket.setPrice(0.0);
        ticket.setQuantity(0);
        ticket.setSession(null);
        return new Ticket();
    }
}
