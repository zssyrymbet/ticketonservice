package kz.iitu.ticketonsessionservice.service;
import kz.iitu.ticketonsessionservice.entity.*;
import java.util.List;
import java.util.Map;

public interface SessionService {
    Session createSession(Session session);
    List<Session> getAllSessions();
    Session getSessionById(Long id);
    ConcertSessions findByConcertIsNotNull();
    MovieSessions findByMovieIsNotNull();
    MusicalSessions findByMusicalIsNotNull();
    MuseumSessions findByMuseumIsNotNull();
    FitnessSessions findByFitnessIsNotNull();
    Map<String, Boolean> deleteSession(Long id);
    Ticket getTicketById(Long id);
}
