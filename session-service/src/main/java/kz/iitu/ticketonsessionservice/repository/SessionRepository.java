package kz.iitu.ticketonsessionservice.repository;
import kz.iitu.ticketonsessionservice.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {
    List<Session> findByConcertIsNotNull();
    List<Session> findByMovieIsNotNull();
    List<Session> findByMusicalIsNotNull();
    List<Session> findByMuseumIsNotNull();
    List<Session> findByFitnessIsNotNull();
    Session getSessionById(Long id);
    Ticket getTicketById(Long id);
}
