package kz.iitu.ticketonclientservice.service;
import kz.iitu.ticketonclientservice.entity.Booking;
import kz.iitu.ticketonclientservice.entity.Client;
import kz.iitu.ticketonclientservice.entity.ClientBooking;

import java.util.List;
import java.util.Map;

public interface ClientService {
    Client createClient(Client client);
    Client getClientById(Long id);
    Client findByUsername(String username);
    List<Client> getAllClients();
    Map<String, Boolean> deleteClient(Long id);
    ClientBooking getBookingById(Long id);
}
