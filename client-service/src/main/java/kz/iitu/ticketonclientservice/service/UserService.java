package kz.iitu.ticketonclientservice.service;
import kz.iitu.ticketonclientservice.entity.User;
import java.util.List;
import java.util.Map;

public interface UserService {
    List<User> getAllUsers();
    User createUser(User user);
    User findById(Long id);
    User findByUsername(String username);
    Map<String, Boolean> deleteUser(Long id);
}
