package kz.iitu.ticketonclientservice.service.impl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.ticketonclientservice.entity.Booking;
import kz.iitu.ticketonclientservice.entity.Client;
import kz.iitu.ticketonclientservice.entity.ClientBooking;
import kz.iitu.ticketonclientservice.repository.ClientRepository;
import kz.iitu.ticketonclientservice.service.ClientService;
import lombok.NonNull;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.apache.commons.codec.binary.Base64;
import java.util.*;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Client createClient(Client client) {
        clientRepository.saveAndFlush(client);
        return client;
    }

    @Override
    public Client getClientById(Long id) {
        return clientRepository.getClientById(id);
    }

    @Override
    public Client findByUsername(String username) {
        @NonNull Client client = clientRepository.findByUsername(username);
        return client;
    }

    @Override
    public List<Client> getAllClients() {
        return (List<Client>) clientRepository.findAll();
    }

    @Override
    public Map<String, Boolean> deleteClient(Long id) {
        clientRepository.findById(id);
        clientRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getFallbackBooking",
            threadPoolKey = "clientBookingInfo",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value="20"),
                    @HystrixProperty(name = "maxQueueSize", value="10")
            }
    )
    public ClientBooking getBookingById(Long id) {

        String apiCredentials = "rest-ticket:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        System.out.println("base64Credentials " + base64Credentials);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

//        return restTemplate.getForObject("http://booking-service/booking/client/" + id, ClientBooking.class).getClientBookingList();

        return restTemplate.exchange("http://booking-service/booking/client/" + id,
                HttpMethod.GET, entity, ClientBooking.class).getBody();

    }

    private ClientBooking getFallbackBooking(Long id) {
        return new ClientBooking();
    }
}
