package kz.iitu.ticketonclientservice.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ClientBooking {

    private List<Booking> clientBookingList;

    public ClientBooking() {}

    public ClientBooking(List<Booking> clientBookingList) {
        this.clientBookingList = clientBookingList;
    }
}
