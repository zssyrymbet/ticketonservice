package kz.iitu.ticketonclientservice.entity;

public enum Type {
    MALE,
    FEMALE,
    STUDENT,
}
