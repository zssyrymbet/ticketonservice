package kz.iitu.ticketonclientservice.entity;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "clients")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private int card_number;

    private double balance;

}
