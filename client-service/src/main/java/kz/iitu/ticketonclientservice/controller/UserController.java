package kz.iitu.ticketonclientservice.controller;
import kz.iitu.ticketonclientservice.entity.User;
import kz.iitu.ticketonclientservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@RequestBody User userForm, BindingResult bindingResult)  {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.createUser(userForm);
        return "redirect:/welcome";
    }

    public String welcome(Model model) {
        return "welcome";
    }

    @GetMapping("/api/users")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }


    @GetMapping("/id/{id}")
    public User findById(@PathVariable(value = "id") Long id) {
        return userService.findById(id);
    }

    @GetMapping("/username/{username}")
    public User findByUsername(@PathVariable(value = "username") String username) {
        return userService.findByUsername(username);
    }

    @DeleteMapping("/admin/delete/users/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long id) {
        return userService.deleteUser(id);
    }

}


