package kz.iitu.ticketonclientservice.controller;
import kz.iitu.ticketonclientservice.entity.Booking;
import kz.iitu.ticketonclientservice.entity.Client;
import kz.iitu.ticketonclientservice.entity.ClientBooking;
import kz.iitu.ticketonclientservice.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/admin/create")
    public void createClient(@RequestBody Client client)  {
         clientService.createClient(client);
    }

    @GetMapping("/id/{id}")
    public Client getClientById(@PathVariable(value = "id") Long id) {
        return clientService.getClientById(id);
    }

    @GetMapping("/booking/{id}")
    public ClientBooking getBookingById(@PathVariable(value = "id") Long id) {
        return clientService.getBookingById(id);
    }

    @GetMapping("/all")
    public List<Client> getAllClients() {
        return clientService.getAllClients();
    }

    @GetMapping("/username/{username}")
    public Client findByUsername(@PathVariable(value = "username") String username) {
        return clientService.findByUsername(username);
    }

    @DeleteMapping("/admin/delete/{id}")
    public Map<String, Boolean> deleteClient(@PathVariable(value = "id") Long id) {
        return clientService.deleteClient(id);
    }

}
