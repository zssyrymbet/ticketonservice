package kz.iitu.ticketonclientservice.repository;
import kz.iitu.ticketonclientservice.entity.Booking;
import kz.iitu.ticketonclientservice.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client getClientById(Long id);
    Client findByUsername(String name);
    List<Booking> getBookingById(Long id);
}
