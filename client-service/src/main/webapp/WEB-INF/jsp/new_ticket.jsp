<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>New Ticket</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/grid/">
	<link rel="stylesheet" type="text/css" href="${spring.mvc.view.prefix}/css/admin.css"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .themed-grid-col {
          padding-top: 8px;
          padding-bottom: 8px;
          background-color: rgba(86, 61, 124, .15);
          border: 1px solid rgba(86, 61, 124, .2);
      }
      .form-control {
          min-height: 41px;
          box-shadow: none;
          border-color: #e6e6e6;
      }
      .form-control:focus {
          border-color: #00c1c0;
      }
      .form-control, .btn {
          border-radius: 3px;
      }
      .signup-form {
          width: 425px;
          margin: 0 auto;
          padding: 30px 0;
      }
      .signup-form h2 {
          color: #333;
          font-weight: bold;
          margin: 0 0 25px;
      }
      .signup-form form {
          margin-top: 70px;
          background: #fff;
          border: 1px solid #f4f4f4;
          box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
          padding: 40px 50px;
      }
      .signup-form .form-group {
          margin-bottom: 20px;
      }
      .signup-form label {
          font-weight: normal;
          font-size: 13px;
      }
      .signup-form input[type="checkbox"] {
          margin-top: 2px;
      }
      .signup-form .btn {
          font-size: 16px;
          font-weight: bold;
          background: #00c1c0;
          border: none;
          min-width: 140px;
          outline: none !important;
      }
      .signup-form .btn:hover, .signup-form .btn:focus {
          background: #00b3b3;
      }
      .signup-form a {
          color: #00c1c0;
          text-decoration: none;
      }
      .signup-form a:hover {
          text-decoration: underline;
      }
      .col-sm-2 {
          max-width: 68% !important;
      }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
         <a class="navbar-brand" href="/concerts">Ticketon Admin</a>
    </nav>
    <div align="center">
        <div class="signup-form">
            <form:form action="save_ticket" method="post" modelAttribute="ticket" class="signup-form">
                  <h2>New Ticket</h2>
                  <div class="form-group">
                        <form:input path="price" class="col-2 col-sm-2 themed-grid-col" placeholder="Price"/>
                  </div>
                  <div class="form-group">
                        <form:input path="quantity" class="col-2 col-sm-2 themed-grid-col" placeholder="Quantity"/>
                  </div>
                  <div class="form-group">
                        <form:input path="session" class="col-2 col-sm-2 themed-grid-col" placeholder="Session"/>
                  </div>
                  <div class="form-group">
                        <form:input path="place" class="col-2 col-sm-2 themed-grid-col" placeholder="Place"/>
                  </div>
                  <div class="form-group">
                        <button type="submit" class="btn btn-primary">Create</button>
                  </div>
            </form:form>
        </div>
    </div>
</body>
</html>