<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Ticketon</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/jumbotron/">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="index.css">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .btn-outline-success {
        margin-right: 10px !important;
      }

      .card {
        margin-right: 25px;
        margin-bottom: 20px;
      }

      .btn-secondary {
        border-color: #00b3b3;
        background-color: #00b3b3;
      }

    </style>
    <link href="jumbotron.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/index">Ticketon</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="/concert_page">Concert</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/fitness_page">Fitness</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/movie_page">Movie</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/museum_page">Museum</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/musical_page">Musical</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/client_page?id=3">Client</a>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                <a class="btn btn-outline-success my-2 my-sm-0" href="/signin" role="button">Sign In</a>
            </form>
      </div>
    </nav>

<div class="container">

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>

    </c:if>

</div>
<main role="main">
  <div class="jumbotron">
    <div class="container">
      <h1 class="display-3">Ticketon</h1>
      <h3>Tickets online for events in Kazakhstan. Poster of all events of Kazakhstan.</h3>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="card" style="width: 18rem;">
        <img style="width: 100%; height: 50%" class="card-img-top" src="https://i.ytimg.com/vi/Z_sUSTj9Go4/hqdefault.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">King and Jester</h5>
          <p class="card-text">Back in 1988, in Leningrad, Mikhail Gorshenyev, along with classmates, had the idea of creating a group.</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details</a></p>
        </div>
      </div>
      <div class="card" style="width: 18rem;">
        <img style="width: 100%; height: 50%" class="card-img-top" src="https://article.images.consumerreports.org/f_auto/prod/content/dam/CRO%20Images%202019/Health/08August/Cr-Health-InlineHero-How-To-Hear-Better-At-Concerts-8-19" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Concert</h5>
          <p class="card-text">Back in 1988, in Leningrad, Mikhail Gorshenyev, along with classmates, had the idea of creating a group.</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details</a></p>
        </div>
      </div>
      <div class="card" style="width: 18rem;">
        <img style="width: 100%; height: 50%" class="card-img-top" src="https://bsp-static.playbill.com/5c/0d/45bfc5d443dead761448df45bd3b/spongebob-squarepants-broadway-production-photos-2017-4-cast-hr.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Musical</h5>
          <p class="card-text">The TV production features much of the original Broadway cast, including Tony nominees Ethan Slater and Gavin Lee.</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details</a></p>
        </div>
      </div>
      <div class="card" style="width: 18rem;">
        <img style="width: 100%; height: 50%" class="card-img-top" src="https://travel4all.org/wp-content/uploads/2019/06/museums-in-the-world.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Museum</h5>
          <p class="card-text">Museums in the world are Sources of attraction Tourists come from around the world to see the antiquities and artifacts.</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details</a></p>
        </div>
      </div>
      <div class="card" style="width: 18rem;">
        <img style="width: 100%; height: 50%" class="card-img-top" src="https://see.news/wp-content/uploads/2020/01/960x0.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Megamind</h5>
          <p class="card-text">Back in 1988, in Leningrad, Mikhail Gorshenyev, along with classmates, had the idea of creating a group.</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details</a></p>
        </div>
      </div>
      <div class="card" style="width: 18rem;">
        <img style="width: 100%; height: 50%" class="card-img-top" src="https://onlinehealth-tips.com/wp-content/uploads/2019/10/08-Fitness-Tips.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Fitness</h5>
          <p class="card-text">Back in 1988, in Leningrad, Mikhail Gorshenyev, along with classmates, had the idea of creating a group.</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details</a></p>
        </div>
      </div>
    </div>
    <hr>
  </div>
</main>

<footer class="container">
  <p>&copy; Ticketon 2020</p>
</footer>
</body>
</html>
